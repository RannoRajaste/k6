import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

/**
 * Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

    /**
     * Main method.
     */
    public static void main(String[] args) {
        GraphTask a = new GraphTask();
        a.run();
    }

    /**
     * Actual main method to run examples and everything.
     */
    private void run() {
        Graph g = new Graph("G");
        g.createRandomSimpleGraph(5, 6);
        try {
            Graph clone = g.clone();

            // Check if graphs are equal

            System.out.println("Graphs are equal? -> " + g.toString().equals(clone.toString()));

            // Check if graph info fields are equal

            System.out.println("Graph info fields are equal? -> " + (clone.first.next.next.info == (g.first.next.next.info)));

            // Change first graph

            g.first.next.id = "AAAA";
            g.first.first.id = "AAAA";
            g.first.next.next = new Vertex("AAAAAAA");
            g.first.next.info = 200;

            // Check if clone is independent i.e clone didn't change

            System.out.println("Clone is independent? -> " + !clone.toString().equals(g.toString()));

            // Check if clone info fields are independent

            System.out.println("Clone info fields are independent? -> " + (clone.first.next.info != g.first.next.info));

            // Check how long a graph with 2500 vertices and 2500 arcs takes to clone


            Graph bigGraph = new Graph("Big");
            bigGraph.createRandomSimpleGraph(2000, 5000);
            long startTime = System.nanoTime();
            bigGraph.clone();
            long endTime = System.nanoTime();
            long duration = (endTime - startTime);
            System.out.println("Time taken to clone Graph with  2500 vertices and 5000 arcs: " + duration / 1000000000.0 + " seconds");
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Vertex represents a point in graph. A vertex contains a reference to next vertex
     * as well as a reference to first arc leaving from said vertex.
     */
    class Vertex {

        private String id;
        private Vertex next;
        private Arc first;
        private int info = 0;

        Vertex(String s, Vertex v, Arc e) {
            id = s;
            next = v;
            first = e;
        }

        Vertex(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }
    }


    /**
     * Arc represents one arrow in the graph. Two-directional edges are
     * represented by two Arc objects (for both directions).
     */
    class Arc {

        private String id;
        private Vertex target;
        private Arc next;
        private int info = 0;

        Arc(String s, Vertex v, Arc a) {
            id = s;
            target = v;
            next = a;
        }

        Arc(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }
    }

    class Graph implements Cloneable {

        private String id;
        private Vertex first;
        private int info = 0;

        Graph(String s, Vertex v) {
            id = s;
            first = v;
        }

        Graph(String s) {
            this(s, null);
        }

        @Override
        public String toString() {
            String nl = System.getProperty("line.separator");
            StringBuffer sb = new StringBuffer(nl);
            sb.append(id);
            sb.append(nl);
            Vertex v = first;
            while (v != null) {
                sb.append(v.toString());
                sb.append(" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append(" ");
                    sb.append(a.toString());
                    sb.append(" (");
                    sb.append(v.toString());
                    sb.append("->");
                    sb.append(a.target.toString());
                    sb.append(")");
                    a = a.next;
                }
                sb.append(nl);
                v = v.next;
            }
            return sb.toString();
        }

        public Vertex createVertex(String vid) {
            Vertex res = new Vertex(vid);
            res.next = first;
            first = res;
            return res;
        }

        public Arc createArc(String aid, Vertex from, Vertex to) {
            Arc res = new Arc(aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
            return res;
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         *
         * @param n number of vertices added to this graph
         */
        public void createRandomTree(int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex[n];
            for (int i = 0; i < n; i++) {
                varray[i] = createVertex("v" + (n - i));
                if (i > 0) {
                    int vnr = (int) (Math.random() * i);
                    createArc("a" + varray[vnr].toString() + "_"
                            + varray[i].toString(), varray[vnr], varray[i]);
                    createArc("a" + varray[i].toString() + "_"
                            + varray[vnr].toString(), varray[i], varray[vnr]);
                } else {
                }
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         *
         * @return adjacency matrix
         */
        public int[][] createAdjMatrix() {
            info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int[info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res[i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Create a connected simple (undirected, no loops, no multiple
         * arcs) random graph with n vertices and m edges
         * @param n number of vertices
         * @param m number of edges
         */
        public void createRandomSimpleGraph(int n, int m) {
            if (n <= 0)
                return;
            if (m < n - 1 || m > n * (n - 1) / 2)
                throw new IllegalArgumentException
                        ("Impossible number of edges: " + m);
            first = null;
            createRandomTree(n);       // n-1 edges created here
            Vertex[] vert = new Vertex[n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            int[][] connected = createAdjMatrix();
            int edgeCount = m - n + 1;  // remaining edges
            while (edgeCount > 0) {
                int i = (int) (Math.random() * n);  // random source
                int j = (int) (Math.random() * n);  // random target
                if (i == j)
                    continue;  // no loops
                if (connected[i][j] != 0 || connected[j][i] != 0)
                    continue;  // no multiple edges
                Vertex vi = vert[i];
                Vertex vj = vert[j];
                createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                connected[i][j] = 1;
                createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
                connected[j][i] = 1;
                edgeCount--;  // a new edge happily created
            }
        }

        /**
         * Deep copy Graph
         *
         * @return A Graph that is a deep copy of original
         */
        @Override
        protected Graph clone() throws CloneNotSupportedException {
            Graph clonedGraph = (Graph) super.clone();

            clonedGraph.info = this.info;
            if (this.first == null) {
                return clonedGraph;
            }

            // Discover all vertices, collect them into list, clone them and collect clones of them into HashMap

            Stack<Vertex> vertexStack = new Stack<>();
            HashMap<String, Vertex> clonedVertices = new HashMap<>();
            Vertex rootVertex = this.first;
            vertexStack.push(rootVertex);
            Vertex clonedParentVertex = clonedGraph.createVertex(rootVertex.id);
            clonedVertices.put(clonedParentVertex.id, clonedParentVertex);
            List<Vertex> foundVertices = new ArrayList<>();
            foundVertices.add(rootVertex);
            while (!vertexStack.isEmpty()) {
                Vertex vertexInProcessing = vertexStack.pop();
                Vertex nextVertex = vertexInProcessing.next;
                while (nextVertex != null) {
                    if (!foundVertices.contains(nextVertex)) {
                       Vertex clonedNextVertex = new Vertex(nextVertex.id);
                       clonedNextVertex.info = nextVertex.info;
                       clonedVertices.put(clonedNextVertex.id, clonedNextVertex);
                       foundVertices.add(nextVertex);
                       clonedParentVertex.next = clonedNextVertex;

                       vertexStack.push(nextVertex);
                       clonedParentVertex = clonedNextVertex;
                    }
                    nextVertex = nextVertex.next;
                }
            }

            // Discover all Arcs for each vertex, collect arcs into a stack, iterate over the stack and clone arcs

            for (Vertex vertex : foundVertices) {
               Stack<Arc> discoveredArcs = new Stack<>();
              Arc nextArc = vertex.first;
              while(nextArc != null) {
                 discoveredArcs.push(nextArc);
                 nextArc = nextArc.next;
              }
               while (!discoveredArcs.isEmpty()) {
                  Arc discoveredArc = discoveredArcs.pop();
                  String id = discoveredArc.id;
                  Vertex source = clonedVertices.get(vertex.id);
                  Vertex target = clonedVertices.get(discoveredArc.target.id);
                  Arc clonedArc = clonedGraph.createArc(id, source, target);
                  clonedArc.info = discoveredArc.info;
               }
           }
           return clonedGraph;
        }
    }
}

